; Municipal Code - Core Makefile

api = 2
core = 7.x

projects[auto_entitylabel][subdir] = contrib
projects[ctools][subdir] = contrib
projects[date][subdir] = contrib
projects[entity][subdir] = contrib
projects[entityconnect][subdir] = contrib
projects[entityreference][subdir] = contrib
projects[entityreference_prepopulate][subdir] = contrib
projects[eva][subdir] = contrib
projects[features][subdir] = contrib
projects[field_collection][subdir] = contrib

projects[file_entity][subdir] = contrib
projects[file_entity][version] = 2.0-beta1

projects[filefield_sources][subdir] = contrib
projects[imce][subdir] = contrib
projects[link][subdir] = contrib

projects[media][version] = 2.x-dev
projects[media][subdir] = contrib
projects[media][download][type] = git
; 7.x-2.0-alpha4+7-dev
projects[media][download][revision] = f5db1d1
projects[media][download][branch] = 7.x-2.x

projects[strongarm][subdir] = contrib
projects[token][subdir] = contrib
projects[views][subdir] = contrib
