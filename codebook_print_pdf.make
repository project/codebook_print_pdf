; Municipal Code - Print/PDF Makefile

api = 2
core = 7.x

projects[features_override][subdir] = contrib
projects[print][subdir] = contrib

libraries[dompdf][download][type] = "get"
libraries[dompdf][download][url] = "https://github.com/dompdf/dompdf/releases/download/v0.6.2/dompdf-0.6.2.zip"
libraries[dompdf][directory_name] = "dompdf"
libraries[dompdf][destination] = "libraries"
